package model1;

public class Person implements Measurable{
	private String name;
	private double height;
	
	public Person(String aName, double aHeight){
		name = aName;
		height = aHeight;
	}
	
	public String getName(){
		return name;
	}
	
	public double getHeight(){
		return height;
	}
	
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return height;
	}
}
