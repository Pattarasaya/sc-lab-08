package model1;

public class BankAccount implements Measurable{
	private String name;
	private double balance;
	
	public BankAccount(String aName, double aBalance){
		name = aName;
		balance = aBalance;
	}
	
	public String getName(){
		return name;
	}
	
	public double getBalance(){
		return balance;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return 0;
	}

}
