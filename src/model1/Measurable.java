package model1;

public interface Measurable {
	double getMeasure();
}
