package model2;

public class Company implements Taxable{
	private String name;
	private double receive;
	private double payment;
	
	public Company(String aName, double anReceive, double anPayment){
		name = aName;
		receive = anReceive;
		payment = anPayment;
	}
	
	public String getName(){
		return name;
	}
	
	public double getReceive(){
		return receive;
	}
	
	public double getPayment(){
		return payment;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double sum = 0;
		sum = (receive-payment)*0.30;
		return sum;
	}
}
