package model2;

public interface Taxable {
	
	double getTax();
}
